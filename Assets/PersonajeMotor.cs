﻿using UnityEngine;
using System.Collections;

	public class PersonajeMotor : MonoBehaviour {

		public float maxVelocidad;

		public bool isCorriendo;

		public float impulsoSalto;

		public float[] carriles;
		public int carrilActual;


		private Rigidbody rigidBody;


		// Use this for initialization
		void Start () {
			rigidBody = GetComponent<Rigidbody>();
		}

		// Update is called once per frame
		void Update () {

		}


		public void Jump(){
			rigidBody.AddForce (Vector3.up * impulsoSalto, ForceMode.Impulse);
			Debug.Log("saltando");
		}

		public void Crouch(){

		}


		public int getCarrilActual(){
			return carrilActual;
		}

		public void MoveLeft(){
			if(getCarrilActual()!=1){
				Vector3 actual = rigidBody.transform.position;
				actual.x+=carriles[getCarrilActual()];
				rigidBody.transform.position = actual;
				carrilActual = carrilActual -1;
			}
		}

		public void MoveRight(){
			if(getCarrilActual()!=3){
				Vector3 actual = rigidBody.transform.position;
				actual.x-=carriles[getCarrilActual()];
				rigidBody.transform.position = actual;
				carrilActual+=1;
			}
		}

	}
