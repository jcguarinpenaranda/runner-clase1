﻿using UnityEngine;
using System.Collections;



	[RequireComponent(typeof(PersonajeMotor))]
	public class PersonajeInput : MonoBehaviour {

		PersonajeMotor motor;

		// Use this for initialization
		void Start () {
			motor = GetComponent<PersonajeMotor>();
		}

		// Update is called once per frame
		void Update () {
			if(Input.GetButtonDown("Jump")){
					motor.Jump();
			}

			if(Input.GetKeyDown(KeyCode.LeftArrow)){
				motor.MoveLeft();
			}

			if(Input.GetKeyDown(KeyCode.RightArrow)){
				motor.MoveRight();
			}

		}
	}
