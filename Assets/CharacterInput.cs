﻿using UnityEngine;
using System.Collections;

namespace Runner.Character{

	[RequireComponent(typeof(CharacterMotor))]
	public class CharacterInput : MonoBehaviour {

		/*
		 * Will the player move also with
		 * the keyboard? true or false
		 */
		public bool useKeyBoard = true;

		/*
		 * If useTrackSystem is enabled, this will
		 * enable the right and left swipe gestures.
		 * 
		 * No matter if useTrackSystem is enabled or 
		 * not, this will make the player jump or crouch
		 * when swipe up or down respectively.
		 */
		public bool useSwipe = true;

		/*
		 * This only works if useTrackSystem is disabled.
		 */
		public bool useAccelerometer = false;

		/*
		 * The sensitivity of the accelerometer
		 * Bigger values make the player go faster
		 * to the right and to the left
		 */
		public float accelerometerMultiplier = 2;


		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}
	}
}
